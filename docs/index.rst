##########################
Docuemtacon de EFB
##########################


Docuemtacon de EFB

Java 8

##########################
Modelos
##########################



Definir modelo
--------------


*  Name: nombre del modelo
*  Desccription: descipcion del modelo


``@Header(Name="pos.session",Desccription="Sesiones")``

CRUD
--------------

**Crear**

Codigo::

	BaseModel Venta = APP.NewModel("pos.ticket");
	Venta.setValue("name","TK002");
	APP.DB().Save(Venta)

**Buscar**


``APP.DB().Find("base.productos")``


##########################
Tipo de campos
##########################



FieldText
---------

*  Title: titulo a mostrar


Ejemplo:
``@FieldText(Title="Folio")``

FieldOneToMany
--------------

*  Title: titulo a mostrar
*  RelField:Campo relacinal
*  Model:Modelo a relacinar

Ejemplo:
``@FieldOneToMany(Title="Ventas",RelField="sesion_id",Model="pos.ticket")``

FieldDecimal
--------------

*  Title: titulo a mostrar

Ejemplo:
``@FieldDecimal(Title="Precio Unitario")``

FieldManyToOne
--------------

*  Title: titulo a mostrar
*  Model:Modelo a relacinar


Ejemplo:
``@FieldManyToOne(Title="Venta",Model="pos.ticket")``



##########################
Vistas
##########################

Windows
--------------
*  Type=TipeView.Windows

Ejemplo::
	@ViewHeader(Id="view_pos_caja",Model="pos.caja",Title="Terminal de punto de ventas",Type=TipeView.Windows)
	public class TerminalPos extends BorderPane { }




##########################
Menu
##########################

Definir el menu
--------------


* Definit la clase menu::
	@MenuHeader
	public class menu { }

Elemento del menu
-----------------

*  Title: Titulo de a mostrar
*  Padre: se indica el menu que pertenesera
*  Accion: se indica la vista a mostrar
Codigo::
	@MenuElement(Title="Sessiones",Padre="root_pos",Accion="accion_pos_session")

Accion de vista
-----------------

*  Title: Titulo de a mostrar
*  Model: Se determina la el modelo a mostrar
*  Tipe: Se detemina el tipo de vista y por default es ``TipeView.List``

Codigo::
	@MenuAccion(Title="Sessiones",Model="pos.ticket")