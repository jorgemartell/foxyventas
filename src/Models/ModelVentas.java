package Models;


import DB.BajaBitField;
import DB.BaseModel;
import View.ViewItem;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

/**
 *
 * @author jorge carlo martell
 */
@Header(Name="pos.ticket",Desccription="Ticket de venta")
public class ModelVentas {
    
    
    @FieldText(Title="Nombre")
    public String name = "";
    
    @FieldText(Title="Valor")
    public String val = "";
    
    @FieldText(Title="Referencia de cliente")
    public String cliente_ref = "";
    
    @FieldText(Title="Referencia de venta")
    public String ref = "";
    
    @FieldText(Title="Fecha")
    public String fecha = "";
    
    @FieldManyToOne(Title="Cliente",Model="base.contacto")
    public int cliente_id = 0;
    
    @FieldManyToOne(Title="Vendedor",Model="base.usuarios")
    public int usuario_id = 0;
    
    @FieldManyToOne(Title="Sesion",Model="pos.session")
    public int sesion_id = 0;
    
    @FieldOneToMany(Title="Productos",RelField="venta_id",Model="pos.ticket.line")
    public String detalles = "";
    
}
