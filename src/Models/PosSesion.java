/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import DB.BaseModel;
import DB.EventType;
import Report.PDF;
import Report.PDFBuild;
import View.MaterView;
import View.TipeView;
import View.ViewAccion;
import java.util.List;
import javafx.scene.Node;

/**
 *
 * @author jorge carlo martell
 */
@Header(Name="pos.session",Desccription="Sesiones")
public class PosSesion {
    
    @FieldText(Title="Folio")
    public String name = "";
    
    @FieldOneToMany(Title="Ventas",RelField="sesion_id",Model="pos.ticket")
    public String ventas = "";
    
    @Button(Label="Iniciar Terminal", Type=EventType.One)
    public void btn_nuevo_one(MaterView mv,BaseModel mod){
        
        mv.ShowModel(new ViewAccion(){
                     @Override
                     public TipeView TipeView() {
                         return TipeView.Windows;
                     }

                     @Override
                     public String Model() {
                         return "pos.caja";
                     }

                 
                 });
    }
    @PDF(ID="devrep",ButtonTitle="Corde de caja")
    public void btn_rep(PDFBuild pdf){
       pdf.Save();
    }
    
}
