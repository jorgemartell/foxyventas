package Models;


import DB.BajaBitField;
import DB.BaseModel;
import View.ViewItem;
import java.util.ArrayList;
import java.util.List;
import java.util.*; 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

/**
 *
 * @author jorge carlo martell
 */
@Header(Name="pos.ticket.line",Desccription="Producto vendidos")
public class ModelVentasDetalle {
    
    @FieldText(Title="Descripcion")
    public String name = "";
    
    @FieldDecimal(Title="Precio Unitario")
    public float precio = 0;
    
    @FieldDecimal(Title="Cantidad")
    public float cantidad = 0;
    
    @FieldDecimal(Title="Importe")
    public float importe = 0;
    
    @FieldManyToOne(Title="Venta",Model="pos.ticket")
    public int venta_id = 0;
    
    @FieldManyToOne(Title="Producto",Model="base.productos")
    public int producto_id = 0;
    
    
}
