/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

/**
 *
 * @author jorge carlo martell
 */
public abstract class LineaProducto {
    public abstract String Descripcion();
    
    public abstract double Precio();
    
    public abstract double Cantidad();
    
    public abstract int Producto();
    
    public double Importe(){
        return this.Cantidad()*this.Precio();
    }
}
