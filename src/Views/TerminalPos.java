/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import DB.BaseModel;
import View.MaterView;
import View.TipeView;
import View.ViewHeader;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 *
 * @author jorge carlo martell
 */
@ViewHeader(Id="view_pos_caja",Model="pos.caja",Title="Terminal de punto de ventas",Type=TipeView.Windows)
public class TerminalPos extends BorderPane {
 
    
    public static MaterView APP;
    
    BaseModel Venta;
    ObservableList<BaseModel> prods =  FXCollections.observableArrayList();
    TableView<LineaProducto> Table = new TableView();
    
    Label Total = new Label("Total: 0.00");
    Label Sub_Total = new Label("Sub-Total: 0.00");
    
    public void LoadTotal(){
        double total = 0;
        for (LineaProducto l : Table.getItems()) {
            total +=l.Importe();
        }
        
        Total.setText(String.format("Total: %s", total));
        Sub_Total.setText(String.format("Sub-Total: %s", total));
    }
    void NuevVanta(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();

        Venta = APP.NewModel("pos.ticket");
        Venta.setValue("fecha",formatter.format(date));
        Venta.setValue("name","TK002");
        Venta.setValue("cliente_id",1);
        Venta.setValue("sesion_id",1);
        
    }
    public TerminalPos(){
        
        NuevVanta();
        Image img = new Image(getClass().getResourceAsStream("/Icons/happy.png"));
        
        BackgroundImage myBI= new BackgroundImage(img,
        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
        BackgroundSize.DEFAULT);
        
        this.setBackground(new Background(myBI));
        
        
        //this.setStyle("-fx-background-color:#ccc");
        
        
        this.setLeft(Left());
        
        
        this.setCenter(Productos());
    }
    VBox Clientes(){
        VBox re = new VBox();
        
        return re;
    }
    TilePane Productos(){
        TilePane  tl = new TilePane();
        
        tl.setPadding(new Insets(10));
        tl.setHgap(8);
        tl.setVgap(8);
        tl.setPrefColumns(4);
        
        for (BaseModel mod: APP.DB().Find("base.productos")){
            Label l = new Label(mod.getValue("name").toString());
            //Model document = new Model();
            //l.setStyle("-fx-font-size: 16;");
            
            l.setStyle("-fx-text-fill:rgb(250,250,250)");
            StackPane p = new StackPane();
            p.getChildren().add(l);
            //-fx-effect: dropshadow(three-pass-box, #ccc, 1, 0, 1, 1);
            
            p.setStyle(" -fx-background-radius: 5 5 5 5; -fx-background-color:rgb(20,200,200)");
            p.setPrefWidth(150);
            p.setPrefHeight(75);
            
            p.onMouseClickedProperty().set(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent arg0)
                {
                    LineaProducto lp = new LineaProducto(){
                        @Override
                        public String Descripcion() {
                            return mod.getValue("name").toString();
                        }

                        @Override
                        public double Precio() {
                            return Double.parseDouble(mod.getValue("precio").toString());
                        }

                        @Override
                        public double Cantidad() {
                            return 1;
                        }

                        @Override
                        public int Producto() {
                            return mod.getId();
                        }
                    
                    };
                    Table.getItems().add(lp);
                    LoadTotal();
                }
            });
            
            tl.getChildren().add(p);
        }
        return tl;
    }
    String ButtonCss(){
     return "-fx-background-color: \n" +
"        #000000,\n" +
"        linear-gradient(#7ebcea, #2f4b8f),\n" +
"        linear-gradient(#426ab7, #263e75),\n" +
"        linear-gradient(#395cab, #223768);\n" +
"    -fx-background-insets: 0,1,2,3;\n" +
"    -fx-background-radius: 3,2,2,2;\n" +
"    -fx-padding: 12 30 12 30;\n" +
"    -fx-text-fill: white;\n" +
"    -fx-font-size: 12px;";
    
    }
    boolean cliente_back = false;
    VBox Acciones(){
        VBox vb = new VBox();
        Button Clientes = new Button("Clientes");
        Clientes.setPrefWidth(250);
        Clientes.setPadding(new Insets(5));
        Clientes.setStyle(ButtonCss());
        vb.getChildren().add(Clientes);
        Clientes.onMouseClickedProperty().set(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent arg0)
            {
                if(cliente_back==false){
                    cliente_back = true;
                    setCenter(Clientes());
                }else{
                    cliente_back = false;
                    setCenter(Productos());
                }
            
            }
        });
        
        
        
        Button Pagar = new Button("Pagar");
        Pagar.setPadding(new Insets(5));
        Pagar.setPrefWidth(250);
        Pagar.setStyle(ButtonCss());
        vb.getChildren().add(Pagar);
        
        
        
        Pagar.onMouseClickedProperty().set(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent arg0)
                {
                    APP.DB().Save(Venta);
                    
                    for (LineaProducto line_tmp :Table.getItems()) {
                        BaseModel line = APP.NewModel("pos.ticket.line");
                        line.setValue("venta_id",Venta.getId());
                        line.setValue("name",line_tmp.Descripcion());
                        line.setValue("producto_id",line_tmp.Producto());
                        APP.DB().Save(line);
                    }
                    Table.getItems().clear();
                    NuevVanta();
                }
            });
        vb.setPadding(new Insets(10,0,0,5));
        return vb;
    }
    VBox Totales(){
        VBox vb = new VBox();
        
        
        Sub_Total.setStyle("-fx-font-size: 20;");
        vb.getChildren().add(Sub_Total);
        
        
        Total.setStyle("-fx-font-size: 20;");
        vb.getChildren().add(Total);
        vb.setPadding(new Insets(10,0,0,50));
        return vb;
    }
    HBox Left_footer(){
        HBox vb = new HBox();
        HBox _vb = new HBox();
        vb.getChildren().add(Acciones());
        
        vb.getChildren().add(Totales());
        vb.setStyle("-fx-background-radius: 5 5 5 5;-fx-background-color:#ffffff");
        _vb.setPadding(new Insets(10,0,0,0));
        vb.setPrefWidth(500);
        _vb.getChildren().add(vb);
        return _vb;
        
    }
    
    HBox Left_header(){
        HBox vb = new HBox();
        TextField jtf = new TextField();
        //jtf.setLabelFloat(true);
        jtf.setPromptText("Buscar producto...");
        
        jtf.setStyle("-fx-background-radius: 10; -fx-pref-width: 480px;");
        vb.getChildren().add(jtf);
        vb.setPadding(new Insets(10));
        
        return vb;
    }
    VBox Left(){
        VBox vb = new VBox();
        vb.setPadding(new Insets(10));
         
        vb.getChildren().add(Left_header());
        Table.setPrefWidth(500);
        Table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        TableColumn Productos= Col(0,"Productos");
        Productos.setMaxWidth( 1f * Integer.MAX_VALUE * 40 );
        Table.getColumns().add(Productos);
        
        
        TableColumn Precio = Col(1,"Precio");
        Precio.setMaxWidth( 1f * Integer.MAX_VALUE * 20 );
        Table.getColumns().add(Precio);
        
        TableColumn Cantidad = Col(2,"Cantidad");
        Cantidad.setMaxWidth( 1f * Integer.MAX_VALUE * 20 );
        Table.getColumns().add(Cantidad);
        
        TableColumn Importe=  Col(3,"Importe");
        Importe.setMaxWidth( 1f * Integer.MAX_VALUE * 20 );
        Table.getColumns().add(Importe);
        
        vb.getChildren().add(Table);
        
        
        vb.getChildren().add(Left_footer());
        return vb;
    }
    String Cel(int index ,Object row){
        LineaProducto re = (LineaProducto)row;
        if(index==0){
            return re.Descripcion();
        }
        if(index==1){
            return String.format("%s",re.Precio());
        }
        if(index==2){
            return String.format("%s",re.Cantidad());
        }
        if(index==3){
            return String.format("%s",re.Importe());
        }
        return "";
    }
    public TableColumn Col(int index,String title){
        TableColumn<LineaProducto, String> c = new TableColumn<>(title);
        
        
        c.setCellValueFactory(
                        param -> new ReadOnlyObjectWrapper<>(Cel(index,param.getValue()))
                );
        return c;
    }
}
