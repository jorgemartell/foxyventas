
import MenuAPI.MenuAccion;
import MenuAPI.MenuElement;
import MenuAPI.MenuHeader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jorge carlo martell
 */
@MenuHeader
public class menu {
    @MenuElement(Title="Punto de ventas",Icon="icons8-automatic-24.png")
    public String root_pos;
    
    @MenuElement(Title="Sessiones",Padre="root_pos",Accion="accion_pos_session")
    public String menu_session;
    
    @MenuAccion(Title="Sessiones",Model="pos.session")
    public String accion_pos_session;
    
    @MenuElement(Title="Ventas",Padre="root_pos",Accion="accion_pos_ticket")
    public String menu_ventas;
    
    @MenuAccion(Title="Sessiones",Model="pos.ticket")
    public String accion_pos_ticket;
    
    @MenuElement(Title="Productos",Padre="root_pos",Accion="accion_base_productos")
    public String menu_base_productos;
    
    @MenuAccion(Title="Productos",Model="base.productos")
    public String accion_base_productos;
}
