
import Models.Metododepago;
import Models.ModelVentas;
import Models.ModelVentasDetalle;
import BaseCharts.IChart;
import DB.BajaBitField;
import DB.BaseModel;
import DB.Record;
import Models.Caja;
import Models.PosSesion;
import Module.IModulo;
import java.util.ArrayList;
import java.util.List;
import View.MaterView;
import View.ViewItem;
import Views.TerminalPos;
import javafx.scene.control.Control;
import javafx.scene.image.Image;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jorge carlo martell
 */
public class Main extends IModulo {

    @Override
    public String Name() {
        return "Punto de ventas";
    }
    @Override
    public String Color() {
        return "#CE584A";
    }
    @Override
    public Image Icon() {
        Image img = new Image(getClass().getResourceAsStream("/Icons/icons8-cash-register-100.png"));
        return img;
    }
    @Override
    public String Ver() {
        return "1.0";
    }


    @Override
    public List<Class> getObject() {
        List<Class> lt = new ArrayList<>();
        lt.add(Metododepago.class);
        lt.add(ModelVentas.class);
        lt.add(ModelVentasDetalle.class);
        lt.add(PosSesion.class);
        lt.add(menu.class);
        lt.add(Caja.class);
        lt.add(TerminalPos.class);
        return lt;
    }

    
    public static MaterView _app;
    @Override
    public void setMasterView(MaterView app) {
        _app = app;
        TerminalPos.APP = app;
        
    }
   

    @Override
    public List<Record> Records() {
        List<Record> r = new ArrayList<Record>();
        
        String html = "<html lang=\"en\">\n" +
"<head>\n" +
"   <title>Test Velocity</title>\n" +
"</head>\n" +
"<body>\n" +
"<h2>This is ${title}</h2>\n" +
"</body>\n" +
"</html>";
        
        r.add(new Record("ventas","base.report").AddFiels("name","Ventas").AddFiels("html",html));
        return r;
    }

    @Override
    public String Descripcion() {
        return "Terminal de punto de ventas";
    }

    
}
